//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L0C" //  Level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        greatestNumber (i: 23, j: 12)
        
        square (num: 5)
        
        numberBack()
        commonDivider(num: 8128)
        
    }
    //Задача 0. Вывести на экран наибольшее из двух чисел 
    func greatestNumber (i: Int, j: Int ) {
        if i > j {
            print ("\(i) больше чем \(j)")
        }
        else {
            print ("j больше i")
        }
    }
    //Задача 1. Вывести на экран квадрат и куб введенного числа 
    func square (num: Int) {
        let square = num * num
        let kub = num * num * num
        print ("в квадрате - \(square)  в кубе - \(kub)")
    }
    //Задача 2. Вывести на экран все числа до заданного и в обратном порядке до 0
    func numberBack () {
        let q = 0
        var t = 3
        for q in 0...3{
            print ("\(q) \(t-q)")
        }
    }
    func commonDivider (num: Int) {
        var i = num
        var j = 1
        var x = 0
        for j in j...i {
            if i%j == 0 {
                print ("результат \(j)")
                x += 1
            }
        }
        print ("x результат \(i) : \(x)")
    }
}

